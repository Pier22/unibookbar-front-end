import { Status } from './models/satus';
import { log } from 'util';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { staff } from './models/staff';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { admin } from './models/admin';
import { map } from "rxjs/operators";
import * as jwt_decode from 'jwt-decode';
import * as moment from "moment";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
   
  constructor(private http: HttpClient,private router:Router) {
  }
    
  loginStaff(username:string, password:string ) {
      this.http.post('http://localhost:3000/staff/login', {username, password})
      .subscribe((res: any) => {
        if (res.code === 200) {
          this.setSession(res)
          this.router.navigateByUrl('/dashboard');
        }
    }, error => {
        if (error['status'] == 400) {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        } else {
            Swal.fire({
                title: 'Username o password errate',
                text: 'Verifica di aver inserito email o password valide',
                heightAuto: false,
                icon: "error"
            })
        }
    });
          
      
      
  }
  
  //BACK_LOG : GESTIONE DI PIU' LOCALI FOR EVOLUTION
  loginAdmin(email:string, password:string ) {
    this.http.post<admin>('http://localhost:3000/admin/login', {email, password})
    .subscribe((res: any) => {
      if (res.code === 200) {
        this.setSession(res)
        this.router.navigateByUrl('/dashboard');
      }
  }, error => {
      if (error['status'] == 400) {
          Swal.fire({
              title: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Username o password errate',
              text: 'Verifica di aver inserito email o password valide',
              heightAuto: false,
              icon: "error"
          })
      }
  });
}

private setSession(authResult) {   
  const expiresAt = moment().add(authResult.expiresIn,'second');
 
  localStorage.setItem('token', authResult.token);
  localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  const token = jwt_decode(localStorage.getItem('token'));
}  

logout() {
  localStorage.removeItem("token");
  localStorage.removeItem("expires_at");
  this.router.navigateByUrl('/auth/login');
}
public isLoggedIn() {
  return moment().isBefore(this.getExpiration());
}

isLoggedOut() {
  return !this.isLoggedIn();
}

getExpiration() {
  const expiration = localStorage.getItem("expires_at");
  const expiresAt = JSON.parse(expiration);
  return moment(expiresAt);
}  


}
