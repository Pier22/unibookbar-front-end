import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  template: `
    <div class="jumbotron text-center">
      <h1>404 Not Found</h1>
      <p>Ops...Forse ti sei perso! che ne dici di tornare alla home? :-)</p>
      <button style="color: white; border-radius: 10px; background-color: #5E72E4; border: 0px double #cccccc;width: 100px; height:30px" (click)="redirectToDashBoard()">HOME</button>
    </div>
  `
})
export class NotFoundComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  redirectToDashBoard(){
    this.router.navigateByUrl('/dashboard');
  }

}
