import { AuthInterceptor } from './../auth.interceptor';
import { map } from 'rxjs/operators';
import { staff } from './../models/staff';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  loggedstaff:any
   idToken = localStorage.getItem("token");
   header = {
    headers: new HttpHeaders()
      .set("Authorization", "Bearer " + this.idToken)
  }

  constructor(private http: HttpClient) {
      this.showProfile
   }

  editAccount(username:string){
    this.http.post<staff>('http://localhost:3000/staff/edit_profile',{username}).subscribe(res=>{
     
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Modifica ',
          text: 'Username modificato con successo',
          confirmButtonText: 'OK'
        })
      }
    }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });
  }

  showProfile(){
     this.http.get<staff>('http://localhost:3000/staff/show_profile',{})
    .subscribe(res => {
      this.loggedstaff=res
    })  
  }
}
