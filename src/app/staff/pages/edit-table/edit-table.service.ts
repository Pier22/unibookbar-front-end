import {Injectable} from '@angular/core';
import {Table} from 'src/app/models/table';
import {HttpClient, HttpParams} from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class EditTableService {
  selectedTable: Table;

  constructor(private http: HttpClient) {

  }

  getTable(uuid_table: string) {
    this.http.get('http://localhost:3000/staff/table_map', {params: new HttpParams().set('uuid_table', uuid_table)})
      .subscribe(res => {
        this.selectedTable = new Table(uuid_table, res['NumberTable'], res['Chairs'], res['Occupied']);
      });
  }

  editTable(uuid_table: string, chairs: number, occupied: number) {
    this.http.post('http://localhost:3000/staff/edit_table', {uuid_table, chairs, occupied}).subscribe(res => {
      if (res) {
        Swal.fire({
          icon: 'success',
          title: 'Modifica ',
          text: 'Tavolo modificato con successo',
          confirmButtonText: 'OK'
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Modifica',
          text: 'Ops... Qualcosa è andato storto',
          confirmButtonText: 'OK'
        });
      }
    });
  }
}
