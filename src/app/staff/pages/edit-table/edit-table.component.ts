import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

import {Router} from '@angular/router';
import {EditTableService} from './edit-table.service';
import { MapTableService } from '../map-table/map-table.service';


@Component({
  selector: 'app-edit-table',
  templateUrl: './edit-table.component.html',
  styleUrls: ['./edit-table.component.scss']
})
export class EditTableComponent implements OnInit {
  title = 'argon-dashboard-angular';
  // editTableForm: FormGroup;
  occupied: any;
  chairs: any;
  numberTable: any;
 editTableForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public mapService: MapTableService, private router: Router,
              public editTableService: EditTableService) {
    this.editTableService.getTable(this.mapService.selectedTable);
  }

  ngOnInit(): void {
    this.editTableForm = this.formBuilder.group({
      numberTable: [''],
      occupied: [''],
      chairs: ['']
    });

  }

  editTable() {
    const val = this.editTableForm.value;
    this.editTableService.editTable(this.mapService.selectedTable,  val.chairs, val.occupied);
  }

  redirectTomapTable() {
    this.router.navigateByUrl('/maptable');
  }

}
