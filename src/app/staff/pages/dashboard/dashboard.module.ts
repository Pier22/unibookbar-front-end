import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {ClipboardModule} from 'ngx-clipboard';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule, Routes} from '@angular/router';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';

const routes: Routes = [];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ClipboardModule,
    MatDatepickerModule,
    MatCheckboxModule
  ],
})

export class DashboardModule {
}
