import {Component, OnInit} from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import { CreatedishComponent } from '../create-dish/createdish.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  redirectToDish() {
    this.router.navigateByUrl('/createdish');
  }

  redirectToMenu() {
    this.router.navigateByUrl('/createmenu');

  }

}
