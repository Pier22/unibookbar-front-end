import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class MapTableService {
  tables:any
  selectedTable:any
  constructor(private http: HttpClient) { 

  }

  getTable(){
    this.http.get('http://localhost:3000/staff/show_tables',{}).subscribe(res=>{
      this.tables=res;
       
    })
  }

  newTable(){
    this.http.post('http://localhost:3000/staff/new_table',{}).subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Creazione ',
          text: 'Tavolo aggiunto con successo',
          confirmButtonText: 'OK'
        })
     }
    })
  }

  
}
