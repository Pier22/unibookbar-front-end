
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EditTableService } from '../edit-table/edit-table.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import { MapTableService } from './map-table.service';

@Component({
  selector: 'app-maptable',
  templateUrl: './maptable.component.html',
  styleUrls: ['./maptable.component.scss']
})
export class MaptableComponent implements OnInit {
  title = 'argon-dashboard-angular';
  tables:[];


  constructor(public mapService: MapTableService ,private router: Router) {
  }

  calendarPlugins =  [dayGridPlugin];

  ngOnInit(): void {
    this.mapService.getTable();

  }


  redirectToCreateTable(){
    this.mapService.newTable();
    this.router.navigateByUrl('/dashboard');

  }


}
