import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaptableComponent } from './maptable.component';

describe('MaptableComponent', () => {
  let component: MaptableComponent;
  let fixture: ComponentFixture<MaptableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaptableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaptableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
