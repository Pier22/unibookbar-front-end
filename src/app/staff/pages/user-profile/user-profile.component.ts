import { AccountService } from './../../account.service';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  accountForm :FormGroup
  loggedstaff:any


  constructor(private formBuilder:FormBuilder,private router : Router, public accountService:AccountService) {

}

  ngOnInit() {
     this.accountService.showProfile();
    this.accountForm= this.formBuilder.group({
     username: ['']
    })
  }

  ngAfterViewInit(): void {
    this.accountService.showProfile();
  }

  editStaff(){
    const val = this.accountForm.value;
    this.accountService.editAccount(val.username);
}

}
