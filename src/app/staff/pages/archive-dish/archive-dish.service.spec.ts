import { TestBed } from '@angular/core/testing';

import { ArchiveDishService } from './archive-dish.service';

describe('ArchiveDishService', () => {
  let service: ArchiveDishService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArchiveDishService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
