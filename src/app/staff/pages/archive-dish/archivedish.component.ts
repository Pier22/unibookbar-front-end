import { ArchiveDishService } from './archive-dish.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-archivedish',
  templateUrl: './archivedish.component.html',
  styleUrls: ['./archivedish.component.scss']
})
export class ArchivedishComponent implements OnInit {
  title = 'argon-dashboard-angular';
  dishes:[];
  uuidselezionato:string;

  constructor(public archiveService: ArchiveDishService ,private router: Router) {

  }

   ngOnInit(): void {
    this.archiveService.getDishes();

  }

  ngAfterViewInit(): void {
    this.archiveService.getDishes();
  }

  redirectToEditDish(uuidDish:string) {
    this.archiveService.uuidSelected=uuidDish
    this.router.navigateByUrl('/editdish');
  }

  deleteDish(uuidDish:string) {
    this.archiveService.deleteDish(uuidDish);
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['archivedish']);
  })
}

  redirectToLocalManagement() {
    this.router.navigateByUrl('/localmanagement');
  }

}

