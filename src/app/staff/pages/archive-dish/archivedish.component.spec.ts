import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedishComponent } from './archivedish.component';

describe('ArchivedishComponent', () => {
  let component: ArchivedishComponent;
  let fixture: ComponentFixture<ArchivedishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivedishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
