import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class ArchiveDishService {
  dishes:any
  categoryName:any
  headers:any
  uuidSelected:string;

  constructor(private http: HttpClient) { 
   
  }
  
  getDishes():any{
    this.http.get('http://localhost:3000/staff/get_all_dishes',{})
      .subscribe(res =>{
        this.dishes=res;
      })
    }
  deleteDish(uuid_dish:string){
      this.http.post('http://localhost:3000/staff/delete_dish',{uuid_dish}).
      subscribe(res=>{
        if (res) {        
          Swal.fire({
            icon: 'success',
            title: 'Elimina Piatto ',
            text: 'Piatto eliminato con successo',
            confirmButtonText: 'OK'
          })
       }else{
        Swal.fire({
          icon: 'error',
          title: 'Elimina Piatto',
          text: 'Ops... Qualcosa è andato storto',
          confirmButtonText: 'OK'
        })
       }
      })
  }
 
   
}


