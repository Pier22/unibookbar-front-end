import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';

@Component({
  selector: 'app-createmenu',
  templateUrl: './createmenu.component.html',
  styleUrls: ['./createmenu.component.scss']
})
export class CreatemenuComponent implements OnInit {
  constructor() {
  }
  title = 'argon-dashboard-angular';

  calendarPlugins = [dayGridPlugin];

  ngOnInit(){
    
  }
}
