import { Allergens } from '../../../models/allergen';
import { Ingredient } from '../../../models/ingredient';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/models/category';
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Injectable({
  providedIn: 'root'
})
export class DishService {
  categories:any
  ingredients:any
  allergens:any

  constructor(private http: HttpClient) {

   }

   getcategories(){     
     this.http.get<Category>('http://localhost:3000/staff/get_categories',{})
    .subscribe(res => {
      this.categories=res
    }) 
   }

   getingredient(){     
     this.http.get<Ingredient>('http://localhost:3000/staff/get_ingredients',{})
    .subscribe(res => {
      this.ingredients=res
    }) 
   /*  return(this.ingredients);   */
   }

   getAllergens(){     
     this.http.get<Allergens>('http://localhost:3000/staff/get_allergens',{})
    .subscribe(res => {
      this.allergens=res
    }) 
   /*  return(this.allergens);  */ 
   }
   //name, price, category, url_image, bkble_conble, ingredients, allergens

   
   createDish(name:string,price:number,category:string,bkble_conble:number,ingredients:[string],allergens:[string],url_image:string){
    
     this.http.post('http://localhost:3000/staff/new_dish',{name,price,category,url_image,bkble_conble,ingredients,allergens})
     .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Creazione ',
          text: 'Piatto creato con successo',
          confirmButtonText: 'OK'
        })
     }
       
     }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });
   }

}
