import { Allergens } from '../../../models/allergen';
import { Ingredient } from '../../../models/ingredient';
import { Category } from '../../../models/category';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DishService } from './dish.service';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-createdish',
  templateUrl: './createdish.component.html',
  styleUrls: ['./createdish.component.scss']
})
export class CreatedishComponent implements OnInit {
  title = 'argon-dashboard-angular';
  dishForm:FormGroup
  categories:any
  ingredients:any
  allergen:any
  selectedCategory:string;
  arrayIngredient:[string];
  arrayAllergen:[string];
  url_image:string;
  i:number

  addCustomUser = []; /* (name) => ({ name: name}); */

  constructor(private formBuilder:FormBuilder,public dishService:DishService, private router : Router ) {

  }

  ngOnInit(){

    this.dishService.getcategories()
     this.dishService.getingredient()
    this.dishService.getAllergens()

    this.dishForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      category:['', Validators.required],
      Ing:[''],
      aller:[''],
      bkble:['',Validators.required],
     });
  }

  createDish(){
    this.arrayIngredient=this.dishForm.value.Ing;
    this.arrayAllergen= this.dishForm.value.aller;

     for(this.i=0;this.i<this.arrayIngredient.length;this.i=this.i+1){
      this.arrayIngredient[this.i]=this.dishForm.value.Ing[this.i]['UuidIngredient'];
    }
    for(this.i=0;this.i<this.arrayAllergen.length;this.i=this.i+1){
      this.arrayAllergen[this.i]=this.dishForm.value.aller[this.i]['UuidAllergen'];
    }

    const val = this.dishForm.value;

    this.url_image="lasagna"
    this.dishService.createDish(val.name,val.price,val.category['UuidCategory'],val.bkble,this.arrayIngredient,this.arrayAllergen,this.url_image)

  }

  exampleCallback(request: XMLHttpRequest) {
    console.log(request.getAllResponseHeaders());
  }
}
