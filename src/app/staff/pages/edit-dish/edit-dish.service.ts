import { Status } from '../../../models/satus';
import { Observable } from 'rxjs';
import { Category } from '../../../models/category';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Dish } from 'src/app/models/dish';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class EditDishService {
  headers:any
  selectedDish:Dish

  constructor(private http: HttpClient) {

   }


   getDishes(uuid_dish:string){
     this.http.get('http://localhost:3000/staff/get_dish_by_uuid',{params: new HttpParams().set('uuid_dish', uuid_dish)})
     .subscribe((res )=>{
       this.selectedDish= new Dish(uuid_dish,res['Dish']['nameDish'],res['Dish']['BkbleConble'],res['Dish']['Price'],res['Dish']['ingredient'],res['Dish']['allergens'],res['Dish']['Urlimage'],res['Dish']['category'],res['Dish']['quantity'])
       console.log(this.selectedDish);
     })
   }

   editDish(uuid_dish:string, name:string, price:number, category:string, url_image:string, bkble_conble:boolean, ingredients:[string], allergens:[string], quantity:string){

     this.http.post('http://localhost:3000/staff/edit_dish',{uuid_dish,name,price,category,url_image,bkble_conble,ingredients,allergens,quantity})
     .subscribe((res )=>{
      if (res) {
        Swal.fire({
          icon: 'success',
          title: 'Modifica ',
          text: 'Piatto modificato con successo',
          confirmButtonText: 'OK'
        })
     }else{
      Swal.fire({
        icon: 'error',
        title: 'Modifica',
        text: 'Ops... Qualcosa è andato storto',
        confirmButtonText: 'OK'
      })
     }
   }, error => {
    if (error['status'] == 400) {
        Swal.fire({
          title:'Errore',
            text: error['error']['message'],
            heightAuto: false,
            icon: "error"
        })
    } else {
        Swal.fire({
            title: 'Errore',
            text: 'Ops... qualcosa è andato storto',
            heightAuto: false,
            icon: "error"
        })
    }
});
  }




}
