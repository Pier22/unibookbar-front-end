import {Allergens} from '../../../models/allergen';
import {DishService} from '../create-dish/dish.service';
import {EditDishService} from './edit-dish.service';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Dish} from 'src/app/models/dish';
import {ArchiveDishService} from '../archive-dish/archive-dish.service';

@Component({
  selector: 'app-editdish',
  templateUrl: './editdish.component.html',
  styleUrls: ['./editdish.component.scss']
})
export class EditdishComponent implements OnInit {
  title = 'argon-dashboard-angular';
  selectedDish: Dish
  editDishForm: FormGroup
  ingredient: any
  categories: any
  allergens: any
  url_image: string
  arrayIngredient: [string];
  arrayAllergen: [string];
  i: number;
  dishes: any;

  constructor(private formBuilder: FormBuilder, private router: Router, public editService: EditDishService, public dishService: DishService, public archiveDishService: ArchiveDishService) {
    this.editService.getDishes(this.archiveDishService.uuidSelected);

    this.dishService.getingredient();
    this.dishService.getAllergens();
    this.dishService.getcategories();
  }

  ngOnInit(): void {
    this.editDishForm = this.formBuilder.group({
      name: [''],
      bkble: [''],
      category: [''],
      quantity: [''],
      price: [''],
      ing: [''],
      allergens: ['']
    })
  }

  editDish() {
    this.arrayIngredient = this.editDishForm.value.ing;
    this.arrayAllergen = this.editDishForm.value.allergens;

    for (this.i = 0; this.i < this.arrayIngredient.length; this.i = this.i + 1) {
      this.arrayIngredient[this.i] = this.editDishForm.value.ing[this.i]['UuidIngredient'];
    }
    for (this.i = 0; this.i < this.arrayAllergen.length; this.i = this.i + 1) {
      this.arrayAllergen[this.i] = this.editDishForm.value.allergens[this.i]['UuidAllergen'];
    }
    this.url_image = "piatto_bello"
    const val = this.editDishForm.value;

    this.editService.editDish(this.archiveDishService.uuidSelected, val.name, val.price, val.category['UuidCategory'], this.url_image, val.bkble, this.arrayIngredient, this.arrayAllergen, val.quantity)
    this.move();
  }

  move() {
    this.router.navigateByUrl('/archivedish');
  }


}

