import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { ClipboardModule } from 'ngx-clipboard';
import { StaffLayoutRoutes } from './staff-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FullCalendarModule } from '@fullcalendar/angular';
import { DashboardComponent } from 'src/app/staff/pages/dashboard/dashboard.component';
import { UserProfileComponent } from 'src/app/staff/pages/user-profile/user-profile.component';
import { TablesComponent } from 'src/app/staff/pages/tables/tables.component';
import { IconsComponent } from 'src/app/staff/pages/icons/icons.component';
import { MapsComponent } from 'src/app/staff/pages/maps/maps.component';
import { ArchivedishComponent } from 'src/app/staff/pages/archive-dish/archivedish.component';
import { CreatedishComponent } from 'src/app/staff/pages/create-dish/createdish.component';
import { CreatemenuComponent } from 'src/app/staff/pages/create-menu/createmenu.component';
import { EditdishComponent } from 'src/app/staff/pages/edit-dish/editdish.component';
import { MaptableComponent } from 'src/app/staff/pages/map-table/maptable.component';
import {EditTableComponent} from "../../staff/pages/edit-table/edit-table.component";


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(StaffLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    ClipboardModule,
    FullCalendarModule,
    NgSelectModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent,
    CreatedishComponent,
    CreatemenuComponent,
    ArchivedishComponent,
    MaptableComponent,
    EditdishComponent,
    EditTableComponent

  ]
})

export class StaffLayoutModule {}
