import { EditTableComponent } from '../../staff/pages/edit-table/edit-table.component';
import {Routes} from '@angular/router';
import { UserProfileComponent } from 'src/app/staff/pages/user-profile/user-profile.component';
import { TablesComponent } from 'src/app/staff/pages/tables/tables.component';
import { IconsComponent } from 'src/app/staff/pages/icons/icons.component';
import { ArchivedishComponent } from 'src/app/staff/pages/archive-dish/archivedish.component';
import { CreatedishComponent } from 'src/app/staff/pages/create-dish/createdish.component';
import { DashboardComponent } from 'src/app/staff/pages/dashboard/dashboard.component';
import { CreatemenuComponent } from 'src/app/staff/pages/create-menu/createmenu.component';
import { EditdishComponent } from 'src/app/staff/pages/edit-dish/editdish.component';
import { MaptableComponent } from 'src/app/staff/pages/map-table/maptable.component';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';

export const StaffLayoutRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'user-profile', component: UserProfileComponent},
  {path: 'tables', component: TablesComponent},
  {path: 'icons', component: IconsComponent},
  {path: 'archivedish', component: ArchivedishComponent},
  {path: 'createdish', component: CreatedishComponent},
  {path: 'createmenu', component: CreatemenuComponent},
  {path: 'maptable', component: MaptableComponent},
  {path: 'editdish', component: EditdishComponent},
  {path: 'edit-table', component: EditTableComponent},
  {path: '**', component: NotFoundComponent},
];
