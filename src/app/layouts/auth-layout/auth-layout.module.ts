import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginComponent } from 'src/app/shared/login/login.component';
import { RegisterComponent } from 'src/app/shared/register/register.component';

import { AdminLayoutModule } from '../admin-layout/admin-layout.module';
import { AuthLayoutRouting } from './auth-layout.routing';
import { AdminLayoutComponent } from '../admin-layout/admin-layout.component';

@NgModule({
  imports: [
    CommonModule,
    AuthLayoutRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    

  ]
})
export class AuthLayoutModule { }
