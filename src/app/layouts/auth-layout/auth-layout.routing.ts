import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/shared/login/login.component';
import { RegisterComponent } from 'src/app/shared/register/register.component';
import { NgModule } from '@angular/core';



export const routes: Routes = [
    { path: 'login',        component: LoginComponent },
    { path: 'register',       component: RegisterComponent },
    
   
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AuthLayoutRouting {}