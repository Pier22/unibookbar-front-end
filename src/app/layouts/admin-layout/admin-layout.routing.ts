import {Routes} from '@angular/router';
import { UsermanagementComponent } from 'src/app/admin/pages/usermanagement/usermanagement.component';
import { StaffmanagementComponent } from 'src/app/admin/pages/staffmanagement/staffmanagement.component';
import { LocalmanagementComponent } from 'src/app/admin/pages/localmanagement/localmanagement.component';
import { AdminmanagmentComponent } from 'src/app/admin/pages/adminmanagment/adminmanagment.component';
import { IngredientsComponent } from 'src/app/admin/pages/ingredients/ingredients.component';
import { AllergensComponent } from 'src/app/admin/pages/allergens/allergens.component';
import { CreateingredientComponent } from 'src/app/admin/pages/create-ingredients/createingredient.component';
import { CreateallergenComponent } from 'src/app/admin/pages/create-allergens/createallergen.component';
import { CreatestaffComponent } from 'src/app/admin/pages/create-staff/createstaff.component';
import { EditallergenComponent } from 'src/app/admin/pages/edit-allergent/editallergen.component';
import { EditingredientComponent } from 'src/app/admin/pages/edit-ingredient/editingredient.component';
import { CreateuserComponent } from 'src/app/admin/pages/create-user/createuser.component';
import { EditstaffComponent } from 'src/app/admin/pages/edit-staff/editstaff.component';
import { ArchivereviewComponent } from 'src/app/admin/pages/archive-review/archivereview.component';
import { EdituserComponent } from 'src/app/admin/pages/edit-user/edituser.component';


export const AdminLayoutRoutes: Routes = [
 
// to delete   {path: 'user-profile', component: UserProfileComponent},
  
  {path: 'archivereview', component: ArchivereviewComponent},
  {path: 'usermanagement', component: UsermanagementComponent},
  {path: 'staffmanagement', component: StaffmanagementComponent},
  {path: 'localmanagement', component: LocalmanagementComponent},
  {path: 'createuser', component: CreateuserComponent},
  {path: 'createstaff', component: CreatestaffComponent},
  {path: 'edituser', component: EdituserComponent},
  {path: 'editstaff', component: EditstaffComponent},
  {path: 'adminmanagement', component: AdminmanagmentComponent},
  {path: 'ingredients', component: IngredientsComponent},
  {path: 'createingredient', component: CreateingredientComponent},
  {path: 'editingredient', component: EditingredientComponent},
  {path: 'allergens', component: AllergensComponent},
  {path: 'createallergen', component: CreateallergenComponent},
  {path: 'editallergen', component: EditallergenComponent},
];
