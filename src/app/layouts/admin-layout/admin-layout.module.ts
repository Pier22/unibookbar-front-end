import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FullCalendarModule } from '@fullcalendar/angular';
import { UserProfileComponent } from 'src/app/admin/pages/user-profile/user-profile.component';
import { TablesComponent } from 'src/app/admin/pages/tables/tables.component';
import { IconsComponent } from 'src/app/admin/pages/icons/icons.component';
import { MapsComponent } from 'src/app/admin/pages/maps/maps.component';
import { UsermanagementComponent } from 'src/app/admin/pages/usermanagement/usermanagement.component';
import { StaffmanagementComponent } from 'src/app/admin/pages/staffmanagement/staffmanagement.component';
import { LocalmanagementComponent } from 'src/app/admin/pages/localmanagement/localmanagement.component';
import { AdminmanagmentComponent } from 'src/app/admin/pages/adminmanagment/adminmanagment.component';


import { NgSelectModule } from '@ng-select/ng-select';
import { CreateingredientComponent } from 'src/app/admin/pages/create-ingredients/createingredient.component';
import { CreateallergenComponent } from 'src/app/admin/pages/create-allergens/createallergen.component';
import { CreatestaffComponent } from 'src/app/admin/pages/create-staff/createstaff.component';
import { EditallergenComponent } from 'src/app/admin/pages/edit-allergent/editallergen.component';
import { EditingredientComponent } from 'src/app/admin/pages/edit-ingredient/editingredient.component';
import { CreateuserComponent } from 'src/app/admin/pages/create-user/createuser.component';
import { EditstaffComponent } from 'src/app/admin/pages/edit-staff/editstaff.component';
import { ArchivereviewComponent } from 'src/app/admin/pages/archive-review/archivereview.component';
import { EdituserComponent } from 'src/app/admin/pages/edit-user/edituser.component';
import { AllergensComponent } from 'src/app/admin/pages/allergens/allergens.component';
import { IngredientsComponent } from 'src/app/admin/pages/ingredients/ingredients.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    ClipboardModule,
    FullCalendarModule,
    NgSelectModule
  ],
  declarations: [
    UserProfileComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent,
    ArchivereviewComponent,
    UsermanagementComponent,
    StaffmanagementComponent,
    LocalmanagementComponent,
    CreateuserComponent,
    CreatestaffComponent,
    EdituserComponent,
    EditstaffComponent,
    AdminmanagmentComponent,
    IngredientsComponent,
    CreateingredientComponent,
    EditingredientComponent,
    AllergensComponent,
    CreateallergenComponent,
    EditallergenComponent,

  ]
})

export class AdminLayoutModule {}
