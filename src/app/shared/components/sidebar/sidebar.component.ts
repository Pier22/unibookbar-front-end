import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminGuard } from '../../admin.guard';
import { AuthService } from 'src/app/auth.service';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Agenda menù',  icon: 'ni ni-book-bookmark text-orange', class: '' },
    { path: '/icons', title: 'Mappa tavoli',  icon: 'ni-pin-3 text-blue', class: '' },
    { path: '/maps', title: 'Archivio piatti',  icon: 'ni ni-archive-2 text-orange', class: '' },
    { path: '/user-profile', title: 'Account',  icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/tables', title: 'Logout',  icon: 'ni ni-button-power text-red', class: '' },
    { path: '/createdish', title: 'create dish',  icon: 'ni ni-button-power text-red', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router,public adminGuard:AdminGuard,public authService:AuthService ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }


  redirectToMapTable() {
    this.router.navigateByUrl('/maptable');
  }

  redirectToAccount() {
    this.router.navigateByUrl('/user-profile');
  }

  redirectToHome() {
    if(this.adminGuard.canActivate()==true){
      this.router.navigateByUrl('/dashboard');
    }
    else{
      this.router.navigateByUrl('/dashboard');
    }
    
  }

  redirectToUser() {
    this.router.navigateByUrl('/usermanagement');

  }

  redirectToStaff() {
    this.router.navigateByUrl('/staffmanagement');
  }
  
  
  redirectToLocal() {
    this.router.navigateByUrl('/localmanagement');
  }

  
  redirectToDish() {
    this.router.navigateByUrl('/createdish');
  }

  
  redirectToArchiveDish() {
    this.router.navigateByUrl('/archivedish');
  }


  redirectToReview() {
    this.router.navigateByUrl('/archivereview');
  }

  redirectToAdmin() {
    this.router.navigateByUrl('/adminmanagement');
  }

  redirectToIngredients() {
    this.router.navigateByUrl('/ingredients');
  }

  
  redirectToAllergens() {
    this.router.navigateByUrl('/allergens');
  }

  redirectToMenu() {
    this.router.navigateByUrl('/createmenu');

  }
}
