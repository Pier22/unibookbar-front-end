import { ReactiveFormsModule, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router, RouterLink} from  '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { Token } from '@angular/compiler/src/ml_parser/lexer';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})


export class LoginComponent implements OnInit, OnDestroy {
   adminStub= "admin";
   staffStub="staff";
   userForm :FormGroup

  constructor(private formBuilder:FormBuilder ,public authService: AuthService, private router : Router) {
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
   });
  }

  //TO EVOLUTION FOR MORE LOCAL
  loginAdmin() {
    const val = this.userForm.value;
    if (val.username && val.password) {
        this.authService.loginAdmin(val.username, val.password)
    }else{
    Swal.fire({
      title: 'Errore',
      text: 'Hai lasciato dei campi non compilati',
      heightAuto: false,
      icon: "error"
  })
  }
}

  login() {
    const val = this.userForm.value;

    if (val.username && val.password) {
        this.authService.loginStaff(val.username, val.password)
    }else{
      Swal.fire({
        title: 'Errore',
        text: 'Hai lasciato dei campi non compilati',
        heightAuto: false,
        icon: "error"
    })
    }
  }

  ngOnDestroy() {
  }


  log_in(){
    this.login();

  }

}
