import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
      const token = jwt_decode(localStorage.getItem('token'));
      if(token.canAdmin==1){
         return true;
      }
      else{
        return false;
      }

    }
      
      
}

