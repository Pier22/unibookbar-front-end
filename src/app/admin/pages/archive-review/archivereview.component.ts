import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArchiveReviewService } from './archivereview.service';


@Component({
  selector: 'app-archivereview',
  templateUrl: './archivereview.component.html',
  styleUrls: ['./archivereview.component.scss']
})
export class ArchivereviewComponent implements OnInit {
  title = 'argon-dashboard-angular';

  constructor(private router: Router, public archiveReviewService:ArchiveReviewService) {
  }

  ngOnInit(): void {
    this.archiveReviewService.getReview();

  }
  ngAfterViewInit(): void {
    this.archiveReviewService.getReview();


  }

  redirectToDelete(uuid_review:string) {
    this.archiveReviewService.deleteReview(uuid_review)
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/archivereview']);

  })
  }
}
