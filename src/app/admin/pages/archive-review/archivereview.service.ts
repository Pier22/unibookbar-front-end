import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class ArchiveReviewService {
  review:any
  headers:any
  uuidSelected:string;

  constructor(private http: HttpClient) { 
   
  }
  
  getReview():any{
    this.http.get('http://localhost:3000/admin/show_all_review',{})
      .subscribe(res =>{
        this.review=res;
        
      })
    }

  deleteReview(uuid_rev:string){
    
      this.http.post('http://localhost:3000/admin/delete_review',{ uuid_rev })
      .subscribe(res=>{
        if (res) {        
          Swal.fire({
            icon: 'success',
            title: 'Elimina Review ',
            text: 'Recensione eliminato con successo',
            confirmButtonText: 'OK'
          })
       }
      })
  }
  
}


