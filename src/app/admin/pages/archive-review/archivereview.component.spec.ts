import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivereviewComponent } from './archivereview.component';

describe('ArchivereviewComponent', () => {
  let component: ArchivereviewComponent;
  let fixture: ComponentFixture<ArchivereviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivereviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivereviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
