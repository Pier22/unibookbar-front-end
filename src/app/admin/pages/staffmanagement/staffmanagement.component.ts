import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffManagementService } from './staff-management.service';

@Component({
  selector: 'app-staffmanagement',
  templateUrl: './staffmanagement.component.html',
  styleUrls: ['./staffmanagement.component.scss']
})
export class StaffmanagementComponent implements OnInit {
  title = 'argon-dashboard-angular';

  constructor(private router: Router,public staffManagementService:StaffManagementService) {
  }

  ngOnInit(): void {
  this.staffManagementService.getStaff();
  }

  ngAfterViewInit(): void {
    this.staffManagementService.getStaff();
  }

  redirectToCreateStaff() {

    this.router.navigateByUrl('/createstaff');
  }

  redirectToEditStaff(uuidStaff:string) {
    this.staffManagementService.uuidSelected=uuidStaff;
    this.router.navigateByUrl('/editstaff');
  }

  redirectToDeleteStaff(uuidStaff:string) {
    this.staffManagementService.deleteStaff(uuidStaff);
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['staffmanagement']);
  })
}

}
