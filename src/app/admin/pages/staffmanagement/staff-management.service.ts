import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class StaffManagementService {
  staff:any
  headers:any
  uuidSelected:string;

  constructor(private http: HttpClient) { 
   
  }
  
  getStaff():any{
    this.http.get('http://localhost:3000/admin/show_all_staff',{})
      .subscribe(res =>{
        this.staff=res;
      })
    }

  deleteStaff(uuid_staff:string){
      this.http.post('http://localhost:3000/admin/delete_staff',{uuid_staff})
      .subscribe(res=>{
     
        if (res) {        
          Swal.fire({
            icon: 'success',
            title: 'Elimina Staff ',
            text: 'Staff eliminato con successo',
            confirmButtonText: 'OK'
          })
       }
      })
  }
  
  editStaff(uuid_staff:string){
    this.http.post('http://localhost:3000/admin/edit_staff',{uuid_staff}).
    subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Elimina Staff ',
          text: 'Staff eliminato con successo',
          confirmButtonText: 'OK'
        })
     }else{
      Swal.fire({
        icon: 'error',
        title: 'Elimina Staff',
        text: 'Ops... Qualcosa è andato storto',
        confirmButtonText: 'OK'
      })
     }
    })
}
   
}


