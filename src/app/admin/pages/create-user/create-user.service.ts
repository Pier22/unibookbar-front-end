import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class CreateUserService {
   user:any
   role:any

  constructor(private http: HttpClient) { }

  getRole(){     
    this.http.get('http://localhost:3000/staff/get_role',{})
   .subscribe(res => {
     this.role=res
     
     
   }) 
  }

  newUser( role:string, name:string, surname:string, username:string, password:string, url_image:string, email: string){
    this.http.post('http://localhost:3000/admin/new_client',{role,name,surname,username,password,url_image,email})
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Creazione ',
          text: 'Cliente creato con successo',
          confirmButtonText: 'OK'
        })
     }
     }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });
  }
}
