import { CreateUserService } from './create-user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.scss']
})
export class CreateuserComponent implements OnInit {
  title = 'argon-dashboard-angular';
  userForm: FormGroup
  url_image:string


  constructor(private router: Router,private formBuilder:FormBuilder,public createUserServices:CreateUserService) {
  }

  ngOnInit(): void {
    this.createUserServices.getRole();
    this.userForm = this.formBuilder.group({
        name:[''],
        surname:[''],
        username:[''],
        password:[''],
        email:[''],
        role:[''],
        //url_image['']
    })

  }

  redirectToUserManagement() {
    this.router.navigateByUrl('/usermanagement');
  }

  createUser(){
    const val= this.userForm.value;
    this.url_image="pippo_franco"
    this.createUserServices.newUser(val.role['UuidRole'],val.name,val.surname,val.username,val.password,this.url_image,val.email);
  }

}
