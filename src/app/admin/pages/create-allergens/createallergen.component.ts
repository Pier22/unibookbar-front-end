import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllergenService } from '../allergens/allergen.service';


@Component({
  selector: 'app-createallergen',
  templateUrl: './createallergen.component.html',
  styleUrls: ['./createallergen.component.scss']
})
export class CreateallergenComponent implements OnInit {

  title = 'argon-dashboard-angular';
  allergenForm:FormGroup
  constructor(private router: Router,public allergenSerivice:AllergenService,private formBuilder:FormBuilder) {
  }

  ngOnInit(): void {
    this.allergenForm= this.formBuilder.group({
      name: ['', Validators.required],

     });

  }

  redirectToAllergens() {

    this.router.navigateByUrl('/allergens');
  }

  redirectToCreateAllergen(){
    const val = this. allergenForm.value;
    this.allergenSerivice.createAllergen(val.name);
    this.router.navigateByUrl('/allergens');
  }

}
