import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateallergenComponent } from './createallergen.component';

describe('CreateallergenComponent', () => {
  let component: CreateallergenComponent;
  let fixture: ComponentFixture<CreateallergenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateallergenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateallergenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
