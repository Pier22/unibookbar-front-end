import { log } from 'util';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
ingredients: any;
selected_uuid:string;

  constructor(private http: HttpClient) { }



  getIngredients(){
    this.http.get('http://localhost:3000/admin/get_ingredient',{})
    .subscribe(res =>{
      this.ingredients=res;
    })
  }


  deleteIngredients(uuid_ingredient:string){
    this.http.post('http://localhost:3000/admin/delete_ingredient',{uuid_ingredient})
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Eliminazione ingrediente',
          text: 'ingrediente eliminato con successo',
          confirmButtonText: 'OK'
        })
     }
    }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
        
       } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });
   }

 editIngredient(uuid_ingredient:string,name:string){
  this.http.post('http://localhost:3000/admin/rename_ingredient',{uuid_ingredient,name})
  .subscribe(res=>{
    if (res) {        
      Swal.fire({
        icon: 'success',
        title: 'Modifica ingrediente',
        text: 'ingrediente Modificato con successo',
        confirmButtonText: 'OK'
      })
   }
  }, error => {
    if (error['status'] == 400) {
        Swal.fire({
          title:'Errore',
            text: error['error']['message'],
            heightAuto: false,
            icon: "error"
        })
    } else {
        Swal.fire({
            title: 'Errore',
            text: 'Ops... qualcosa è andato storto o ingrediente gia presente',
            heightAuto: false,
            icon: "error"
        })
    }
});
 }

 createIngredient(name:string){
  this.http.post('http://localhost:3000/admin/new_ingredient',{name})
  .subscribe(res=>{
    if (res) {        
      Swal.fire({
        icon: 'success',
        title: 'Crea Ingrediente',
        text: 'ingrediente inserito con successo',
        confirmButtonText: 'OK'
      })
   }
  }, error => {
    if (error['status'] == 400) {
        Swal.fire({
          title:'Errore',
            text: error['error']['message'],
            heightAuto: false,
            icon: "error"
        })
    } else {
        Swal.fire({
            title: 'Errore',
            text: 'Ops... qualcosa è andato storto o ingrediente gia presente',
            heightAuto: false,
            icon: "error"
        })
    }
});
 }

}
