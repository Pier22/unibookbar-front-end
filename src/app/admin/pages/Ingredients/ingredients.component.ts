
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IngredientService } from './ingredients.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit {
  title = 'argon-dashboard-angular';

  constructor(private router: Router,public ingredientService:IngredientService,) {

  }

  ngOnInit(): void {

   this.ingredientService.getIngredients();
  }

  ngAfterViewInit(): void {
    this.ingredientService.getIngredients();
  }

  redirectToCreateIngredient() {
    this.router.navigateByUrl('/createingredient');
  }

  redirectToEditIngredient(uuidIngredient:string) {
    this.ingredientService.selected_uuid=uuidIngredient;
    this.router.navigateByUrl('/editingredient');
  }

  redirectToDeleteIngredient(uuidIngredient:string) {
    this.ingredientService.deleteIngredients(uuidIngredient);
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
    this.router.navigate(['/ingredients']);
  })
}

}
