import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminmanagment',
  templateUrl: './adminmanagment.component.html',
  styleUrls: ['./adminmanagment.component.scss']
})
export class AdminmanagmentComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  
  redirectToCreateAdmin() {
    this.router.navigateByUrl('/createadmin');
  }

  redirectToEditAdmin() {
    this.router.navigateByUrl('/editadmin');
  }


}
