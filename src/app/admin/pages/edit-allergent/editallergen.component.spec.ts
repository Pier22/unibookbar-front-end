import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditallergenComponent } from './editallergen.component';

describe('EditallergenComponent', () => {
  let component: EditallergenComponent;
  let fixture: ComponentFixture<EditallergenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditallergenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditallergenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
