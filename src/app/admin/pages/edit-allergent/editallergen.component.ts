import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AllergenService } from '../allergens/allergen.service';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

@Component({
  selector: 'app-editallergen',
  templateUrl: './editallergen.component.html',
  styleUrls: ['./editallergen.component.scss']
})
export class EditallergenComponent implements OnInit {
  title = 'argon-dashboard-angular';
   allergenForm:FormGroup
   
  constructor(private router: Router, public allergenService:AllergenService,private formBuilder:FormBuilder) {
   
    
   
  }

  ngOnInit(): void {
    this.allergenForm= this.formBuilder.group({
      name: ['', Validators.required],
      
     });
    
  }

  ngAfterViewInit(): void {
    this.allergenService.getAllergens();
  }

  redirectToAllergens() {
    this.router.navigateByUrl('/allergens');
  }

  redirectToEditAllergen() {
    const val = this.allergenForm.value;
    
    this.allergenService.editAllergen(this.allergenService.selected_uuid,val.name);
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['allergens']);
    }); 
  }


}
