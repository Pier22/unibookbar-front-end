import { log } from 'util';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IngredientService } from '../ingredients/ingredients.service';



@Component({
  selector: 'app-editingredient',
  templateUrl: './editingredient.component.html',
  styleUrls: ['./editingredient.component.scss']
})
export class EditingredientComponent implements OnInit {
  title = 'argon-dashboard-angular';
  ingredientForm:FormGroup

  constructor(private router: Router,private formBuilder:FormBuilder,public ingredientService:IngredientService) {
  }

  ngOnInit(): void {
    this.ingredientForm = this.formBuilder.group({
      name: ['', Validators.required],
      
     });
  }

  ngAfterViewInit(): void {
    this.ingredientService.getIngredients();
  }
 
  redirectToIngredients() {
    this.router.navigateByUrl('/ingredients');
  }

  redirectToEditIngredient() {
    const val = this.ingredientForm.value;
    
    this.ingredientService.editIngredient(this.ingredientService.selected_uuid,val.name);
   
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['ingredients']);
    }); 
  }

}
