import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AllergenService } from './allergen.service';


@Component({
  selector: 'app-allergens',
  templateUrl: './allergens.component.html',
  styleUrls: ['./allergens.component.scss']
})
export class AllergensComponent implements OnInit {
  title = 'argon-dashboard-angular';

  constructor(private router: Router, public allergensService:AllergenService) {
  }

  ngOnInit(): void {
this.allergensService.getAllergens();

  }

  ngAfterViewInit(): void {
    this.allergensService.getAllergens();
  }

  redirectToCreateAllergen() {
    this.router.navigateByUrl('/createallergen');
  }

  redirectToDeleteAllergen(uuidAllergen:string) {
    this.allergensService.deleteAllergen(uuidAllergen)
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/allergens']);
  })
}


  redirectToEditAllergen(uuidAllergen:string) {
    this.allergensService.selected_uuid=uuidAllergen;
    this.router.navigateByUrl('/editallergen');
  }
  
}
