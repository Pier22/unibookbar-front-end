import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class AllergenService {
allergens:any;
selected_uuid:string;

  constructor(private http:HttpClient) { }


  getAllergens():any{
    this.http.get('http://localhost:3000/admin/get_allergens',{})
      .subscribe(res =>{
        this.allergens=res;
      })
    }
  
  deleteAllergen(uuid_allergen:string){
      this.http.post('http://localhost:3000/admin/delete_allergen',{uuid_allergen})
      .subscribe(res=>{
        if (res) {        
          Swal.fire({
            icon: 'success',
            title: 'Eliminazione allergene',
            text: 'allergene eliminato con successo',
            confirmButtonText: 'OK'
          })
       }
      }, error => {
        if (error['status'] == 400) {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        } else {
          Swal.fire({
            title: 'Errore',
            text: 'Ops... qualcosa è andato storto',
            heightAuto: false,
            icon: "error"
        })
        }
    });
  }
  
  editAllergen(uuid_allergen:string,name:string){
    this.http.post('http://localhost:3000/admin/rename_allergen',{uuid_allergen,name})
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Eliminazione allergene',
          text: 'allergene eliminato con successo',
          confirmButtonText: 'OK'
        })
     }
    }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
        Swal.fire({
          title: 'Errore',
          text: 'Ops... qualcosa è andato storto o allergene gia presente',
          heightAuto: false,
          icon: "error"
      })
      }
  });
  
  }
  
  createAllergen(name:string){
    this.http.post('http://localhost:3000/admin/new_allergen',{name})
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Creazione allergene',
          text: 'allergene inserito con successo',
          confirmButtonText: 'OK'
        })
     }
    }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto o ingrediente gia presente',
              heightAuto: false,
              icon: "error"
          })
      }
  });
  }
}
