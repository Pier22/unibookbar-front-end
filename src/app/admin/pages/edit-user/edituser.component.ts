
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EditUserService } from './edit-user.service';
import { UserManagementService } from '../usermanagement/user-management.service';
import { CreateUserService } from '../create-user/create-user.service';


@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss']
})
export class EdituserComponent implements OnInit {
  title = 'argon-dashboard-angular';
  userForm:FormGroup;
  url_image:string

  constructor(private router: Router,private formBuilder:FormBuilder,public editUserServices:EditUserService,public userManagementService:UserManagementService,public createUserServices:CreateUserService) {
  }

  ngOnInit(): void {
          
    this.editUserServices.getUser(this.userManagementService.uuidSelected);
    this.createUserServices.getRole();
    this.userForm= this.formBuilder.group({
        name:[''],
        surname:[''],
        username:[''],
        password:[''],
        role:[''],
        
    })
  }

  
  redirectToUserManagement() {
    this.router.navigateByUrl('/usermanagement');
  }

  editUser(){
    this.url_image="url_image"
    const val= this.userForm.value;
    this.editUserServices.editUser(this.userManagementService.uuidSelected,val.role['UuidRole'],val.name,val.surname,val.username,val.password,this.url_image)
    this.userManagementService.getClients();
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
    this.router.navigate(['usermanagement']);
      
    }); 

  }

}

