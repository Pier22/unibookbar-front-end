import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class EditUserService {
  user:any

  constructor(private http: HttpClient) { }

  getUser(uuid_client:string){
    this.http.get('http://localhost:3000/admin/show_client',{params: new HttpParams().set('uuid_client', uuid_client)}).subscribe(res=>{
      this.user=res;
      
    })
  }

  editUser(uuid_client:string, role:string, name:string, surname:string, username:string, password:string, url_image:string){
    this.http.post('http://localhost:3000/admin/edit_client',{uuid_client,role,name,surname,username,password,url_image})
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Modifica ',
          text: 'Cliente modificato  con successo',
          confirmButtonText: 'OK'
        })
     }
     }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });  
  }
}
