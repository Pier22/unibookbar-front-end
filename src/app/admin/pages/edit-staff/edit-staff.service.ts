import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EditStaffService {
  staff:any

  constructor(private http: HttpClient) { }

  getStaff(uuid_staff:string){
    this.http.get('http://localhost:3000/admin/show_staff',{params: new HttpParams().set('uuid_staff', uuid_staff)})
    .subscribe(res=>{
      this.staff=res
      
    })
    
  }
  editStaff(uuid_staff:string, name:string, surname:string, username:string, password:string, can_admin:number){
    this.http.post('http://localhost:3000/admin/edit_staff',{ uuid_staff, name, surname, username, password, can_admin })
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Modifica ',
          text: 'Staff modificato  con successo',
          confirmButtonText: 'OK'
        })
     }
     }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  });  
  }
}
