import { TestBed } from '@angular/core/testing';

import { EditStaffService } from './edit-staff.service';

describe('EditStaffService', () => {
  let service: EditStaffService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditStaffService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
