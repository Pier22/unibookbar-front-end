import { StaffManagementService } from '../staffmanagement/staff-management.service';
import { EditStaffService } from './edit-staff.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-editstaff',
  templateUrl: './editstaff.component.html',
  styleUrls: ['./editstaff.component.scss']
})
export class EditstaffComponent implements OnInit {
  title = 'argon-dashboard-angular';
  staffForm:FormGroup;


  constructor(private router: Router,private formBuilder:FormBuilder,public editStaffServices:EditStaffService,public staffServices:StaffManagementService) {
  }

  ngOnInit(): void {
  this.editStaffServices.getStaff(this.staffServices.uuidSelected);
  this.staffForm= this.formBuilder.group({
        name:[''],
        surname:[''],
        username:[''],
        password:[''],
        canAdmin:['']

    })
  }

  onItemChange(value){


 }

  redirectToStaffManagement() {
    this.router.navigateByUrl('/staffmanagement');
  }

  editStaff(){
    const val= this.staffForm.value;

    this.editStaffServices.editStaff(this.staffServices.uuidSelected,val.name,val.surname,val.username,val.password,val.canAdmin)
    this.redirectToStaffManagement();
  }
}
