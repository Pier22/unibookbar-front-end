import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatestaffComponent } from './createstaff.component';

describe('CreatestaffComponent', () => {
  let component: CreatestaffComponent;
  let fixture: ComponentFixture<CreatestaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatestaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatestaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
