import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class CreateStaffService {

  constructor(private http: HttpClient) { }

  newStaff(name:string, surname:string, username:string, password:string){
    this.http.post('http://localhost:3000/admin/new_staff',{name, surname, username, password })
    .subscribe(res=>{
      if (res) {        
        Swal.fire({
          icon: 'success',
          title: 'Creazione ',
          text: 'Staff creato con successo',
          confirmButtonText: 'OK'
        })
     }
     }, error => {
      if (error['status'] == 400) {
          Swal.fire({
            title:'Errore',
              text: error['error']['message'],
              heightAuto: false,
              icon: "error"
          })
      } else {
          Swal.fire({
              title: 'Errore',
              text: 'Ops... qualcosa è andato storto',
              heightAuto: false,
              icon: "error"
          })
      }
  }); 
  }
}
