import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup } from '@angular/forms';
import { CreateStaffService } from './create-staff.service';

@Component({
  selector: 'app-createstaff',
  templateUrl: './createstaff.component.html',
  styleUrls: ['./createstaff.component.scss']
})
export class CreatestaffComponent implements OnInit {
  title = 'argon-dashboard-angular';
  staffForm:FormGroup

  constructor(private router: Router,private formBuilder:FormBuilder,public createStaffServices: CreateStaffService) {
  }

  ngOnInit(): void {
  this.staffForm= this.formBuilder.group({
    name:[''],
    surname:[''],
    username:[''],
    password:['']
  })

  }
  redirectToStaffManagement() {
    this.router.navigateByUrl('/staffmanagement');
  }
   createStaff(){
     const val= this.staffForm.value;
    this.createStaffServices.newStaff(val.name,val.surname,val.username,val.password)
   this.redirectToStaffManagement()
  }
}
