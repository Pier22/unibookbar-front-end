import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserManagementService } from './user-management.service';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss']
})
export class UsermanagementComponent implements OnInit  {

  title = 'argon-dashboard-angular';

  clients:[];
  uuidselezionato:string;

  constructor(private router: Router,public userManagementService:UserManagementService) {
  }

  ngOnInit(){
   this.userManagementService.getClients();

  }

  ngAfterViewInit() {
    this.userManagementService.getClients();

  }

  redirectToCreateUser() {
    this.router.navigateByUrl('/createuser');
  }

  redirectToEditUser(uuidClient:string) {
    this.userManagementService.uuidSelected=uuidClient;
    this.router.navigateByUrl('/edituser');
  }

  redirectToDeleteUser(uuidClient:string){
    this.userManagementService.deleteClients(uuidClient);
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
    this.router.navigate(['usermanagement']);

  });

  }

}
