import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  clients:any
  headers:any
  uuidSelected:string;

  constructor(private http: HttpClient) { 
   
  }
  
  getClients():any{
    this.http.get('http://localhost:3000/admin/show_clients',{})
      .subscribe(res =>{
        this.clients=res;
      })
    }

  deleteClients(uuid_client:string){
      this.http.post('http://localhost:3000/admin/delete_client',{uuid_client})
      .subscribe(res=>{
        
        if (res) {        
          Swal.fire({
            icon: 'success',
            title: 'Elimina cliente ',
            text: 'Cliente eliminato con successo',
            confirmButtonText: 'OK'
          })
       }
      })
  }
  
  editClients(uuid_client:string){
    this.http.post('http://localhost:3000/admin/edit_client',{uuid_client}).
    subscribe(res=>{
      if (res) {     
        Swal.fire({
          icon: 'success',
          title: 'Elimina utente ',
          text: 'utente eliminato con successo',
          confirmButtonText: 'OK'
        })
     }else{
      Swal.fire({
        icon: 'error',
        title: 'Elimina utente',
        text: 'Ops... Qualcosa è andato storto',
        confirmButtonText: 'OK'
      })
     }
    })
}
 
   
}


