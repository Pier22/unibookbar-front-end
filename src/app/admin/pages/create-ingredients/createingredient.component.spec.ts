import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateingredientComponent } from './createingredient.component';

describe('CreateingredientComponent', () => {
  let component: CreateingredientComponent;
  let fixture: ComponentFixture<CreateingredientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateingredientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateingredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
