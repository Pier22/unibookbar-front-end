import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IngredientService } from '../ingredients/ingredients.service';



@Component({
  selector: 'app-createingredient',
  templateUrl: './createingredient.component.html',
  styleUrls: ['./createingredient.component.scss']
})
export class CreateingredientComponent implements OnInit {
  ingredientForm:FormGroup
  title = 'argon-dashboard-angular';

  constructor(private formBuilder:FormBuilder ,private router: Router,public ingredientsService:IngredientService) {

  }

  ngOnInit(): void {
    this.ingredientForm = this.formBuilder.group({
      name: ['', Validators.required],

     });
  }

  redirectToIngredients() {
    this.router.navigateByUrl('/ingredients');
  }

createIngredients(){
  const val = this.ingredientForm.value;
  this.ingredientsService.createIngredient(val.name);
  this.router.navigateByUrl('/ingredients');
}

}
