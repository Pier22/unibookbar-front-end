import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalmanagementComponent } from './localmanagement.component';

describe('LocalmanagementComponent', () => {
  let component: LocalmanagementComponent;
  let fixture: ComponentFixture<LocalmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
