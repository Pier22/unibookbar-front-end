import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-localmanagement',
  templateUrl: './localmanagement.component.html',
  styleUrls: ['./localmanagement.component.scss']
})
export class LocalmanagementComponent implements OnInit {
  title = 'argon-dashboard-angular';

  constructor(private router: Router) {
  }

  ngOnInit(): void {

  }

  
  redirectToDish() {
    this.router.navigateByUrl('/createdish');
  }

  
  redirectToArchiveDish() {
    this.router.navigateByUrl('/archivedish');
  }


  redirectToReview() {
    this.router.navigateByUrl('/archivereview');
  }

}
