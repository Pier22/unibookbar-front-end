
import { AuthInterceptor } from './auth.interceptor';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app.routing';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ComponentsModule } from './shared/components/components.module';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { StaffLayoutComponent } from './layouts/staff-layout/staff-layout.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgSrcModule } from 'ng-src';

import { CommonModule } from '@angular/common';
import { AuthGuard } from './shared/auth.guard';
import { AdminGuard } from './shared/admin.guard';
import {NotFoundComponent} from "./not-found/not-found.component";





@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    FullCalendarModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule  ,
    NgSelectModule,
    NgSrcModule
  ],
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    AdminLayoutComponent,
    StaffLayoutComponent,
    NotFoundComponent

  ],
  providers: [ AuthGuard, AdminGuard,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthInterceptor,
      multi:true,

    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
