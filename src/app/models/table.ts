export class Table {
    UuidTable: string;
    NumberTable: number;
    Chairs: number;
    Occupied: boolean;
    
    constructor(uuidTable: string,numberTable: number,chairs:number,occupied:boolean) {
        this.UuidTable=uuidTable;
        this.NumberTable=numberTable;
        this.Occupied=occupied;
        this.Chairs=chairs;
    }
}