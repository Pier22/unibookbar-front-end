import { Category } from './category';
import { Allergens } from './allergen';
import { Ingredient } from './ingredient';
export class Dish {
    UuidDish: string;
    Name: string;
    Price: number;
    Quantity:number;
    BkbleConble: number;
    Ingredient:[];
    Allergens:[];
    UrlImage:string;
    Category:string;


    constructor(uuiddish: string, name: string,bkbleconble: number, price: number,ingredient:[],allergens:[],  urlimage: string ,category:string,quantity:number) {
        this.UuidDish = uuiddish;
        this.Name = name;
        this.BkbleConble = bkbleconble;
        this.Ingredient = ingredient;
        this.Allergens= allergens;
        this.Price = price;
        this.Category = category;
        this.UrlImage = urlimage; 
        this.Quantity= quantity;
    }
}